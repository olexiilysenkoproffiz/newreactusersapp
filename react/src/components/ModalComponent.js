import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Input from "./Input";
import MyButton from "./Button";

export default class ModalComponent extends React.Component {
  render() {
    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  {/* <label>FirstName:</label> */}
                  <Input
                    autoFocus
                    type={"text"}
                    //   title={"FirstName"}
                    name={"firstname"}
                    value={this.props.newUser.firstname}
                    placeholder={"Enter Firstname"}
                    handleChange={this.props.onInput}
                  />{" "}
                </div>
              </div>
              <div className="row">
                <div className="form-group col-md-10">
                  {/* <label>LastName:</label> */}
                  <Input
                    type={"text"}
                    //   title={"LastName"}
                    name={"lastname"}
                    value={this.props.newUser.lastname}
                    placeholder={"Enter Lastname"}
                    handleChange={this.props.onInput}
                  />{" "}
                </div>
              </div>
              <div className="row text-center">
                <div className="form-group col-md-10">
                  {/* <label>Email:</label> */}
                  <Input
                    type={"text"}
                    //   title={"Email"}
                    name={"email"}
                    value={this.props.newUser.email}
                    placeholder={"Enter E-mail"}
                    handleChange={this.props.onInput}
                  />{" "}
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"primary"}
                title={this.props.buttonTitle}
                style={buttonStyle}
              />
              <Button color="danger" onClick={this.props.onToggle}>
                Cancel
              </Button>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
