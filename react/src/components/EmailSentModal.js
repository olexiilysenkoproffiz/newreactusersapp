import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import MyButton from "./Button";

export default class EmailSentModal extends React.Component {
  render() {
    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  <h4>
                    Dear User! The message with further instructions has been
                    sent to your email.
                  </h4>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"info"}
                title={"Done!"}
                style={buttonStyle}
              />
              {/* <Button color="danger" onClick={this.props.onToggle}>
                Cancel
              </Button> */}
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
