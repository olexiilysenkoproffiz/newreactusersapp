import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Input from "./Input";
import MyButton from "./Button";

import { strengthIndicator, strengthColor } from "../utils/StrengthPassword";

export default class SignUpModal extends React.Component {
  render() {
    const strength = strengthIndicator(this.props.newUser.password);
    const color = strengthColor(strength);
    let passwordStrength;
    const colorToStrength = {
      red: "very weak",
      yellow: "weak",
      orange: "normal",
      lightgreen: "strong",
      green: "very strong"
    };
    passwordStrength = colorToStrength[color];

    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  <Input
                    type={"text"}
                    title={"Email:"}
                    name={"email"}
                    value={this.props.newUser.email}
                    placeholder={"Enter email"}
                    handleChange={this.props.onInput}
                    error={this.props.error && this.props.error.email}
                  />
                </div>
              </div>
              <div className="row">
                <div className="form-group col-md-10">
                  <Input
                    type={"password"}
                    title={"Password:"}
                    name={"password"}
                    value={this.props.newUser.password}
                    placeholder={"Enter password"}
                    handleChange={this.props.onInput}
                    style={{ borderColor: color }}
                    className="password-input"
                    error={this.props.error && this.props.error.password}
                  />{" "}
                </div>
              </div>

              <div>
                {
                  <h6
                    style={{
                      borderColor: color,
                      color: color,
                      marginTop: "-20px"
                    }}
                  >
                    {passwordStrength}
                  </h6>
                }
              </div>
              <ul>
                <div className="modal-errors my-modal-error">
                  {this.props.error && Object.entries(this.props.error).map((error, index) => {
                    return <div key={index}>* {error[1]}</div>;
                  })}
                </div>
              </ul>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"primary"}
                title={"Sign Up"}
              />
              <Button color="danger" onClick={this.props.onToggle}>
                Cancel
              </Button>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}
