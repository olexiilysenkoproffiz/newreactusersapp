import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Input from "./Input";
import MyButton from "./Button";

const ResetPasswordModal = props => {
  return (
    <div>
      <Modal isOpen={props.modal}>
        <form>
          <ModalHeader>{props.modalHeader}</ModalHeader>
          <ModalBody>
            <div className="row">
              <div className="form-group col-md-10">
                {/* <label>Email:</label> */}
                <Input
                  type={"password"}
                  title={"Password:"}
                  name={"password"}
                  value={props.newUser.password}
                  placeholder={"Enter New Password"}
                  handleChange={props.onInput}
                  error={props.error && props.error.password}
                />{" "}
              </div>
            </div>
            <div className="row">
              <div className="form-group col-md-10">
                {/* <label>Password:</label> */}
                <Input
                  type={"password"}
                  title={"Confirm Password:"}
                  name={"passwordConfirm"}
                  value={props.newUser.passwordConfirm}
                  placeholder={"Enter password"}
                  handleChange={props.onInput}
                  error={props.error && props.error.passwordConfirm}
                />{" "}
              </div>
            </div>
            <ul>
              <div className="modal-errors" style={errorColor}>
                {props.error && Object.entries(props.error).map((error, index) => {
                  return <div key={index}>* {error[1]}</div>;
                })}
              </div>
            </ul>
          </ModalBody>
          <ModalFooter>
            <MyButton
              action={props.onFormSubmit}
              type={"primary"}
              title={"Save Password"}
              style={buttonStyle}
            />
            <Button color="danger" onClick={props.onToggle}>
              Cancel
            </Button>
          </ModalFooter>
        </form>
      </Modal>
    </div>
  );
};

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
const errorColor = {
  color: "red",
  marginLeft: "-40px"
};

export default ResetPasswordModal;
