import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Input from "./Input";
import MyButton from "./Button";

export default class VerificationModal extends React.Component {
  render() {
    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  {/* <label>SecretToken:</label> */}
                  <Input
                    autoFocus
                    type={"text"}
                    name={"secretToken"}
                    value={this.props.newUser.secretToken}
                    placeholder={"Enter secretToken from the received email"}
                    handleChange={this.props.onInput}
                    error={this.props.error && this.props.error.secretToken}
                  />{" "}
                </div>
              </div>
              <ul>
                <div className="modal-errors" style={errorColor}>
                  {this.props.error && Object.entries(this.props.error).map((error, index) => {
                    return <div key={index}>* {error[1]}</div>;
                  })}
                </div>
              </ul>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"primary"}
                title={"Verify"}
                style={buttonStyle}
              />
              <Button color="danger" onClick={this.props.onToggle}>
                Cancel
              </Button>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

const errorColor = {
  color: "red",
  marginLeft: "-40px"
};
