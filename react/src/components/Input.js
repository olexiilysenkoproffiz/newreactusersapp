import React from "react";

const Input = props => {
  return (
    <div className="form-group">
      <label htmlFor={props.name} className="form-label">
        {props.title}
      </label>
      <input
        className={"form-control " + (props.className ? props.className : "")}
        id={props.name}
        name={props.name}
        type={props.type}
        value={props.value}
        onChange={props.handleChange}
        placeholder={props.placeholder}
        style={props.style}
      />
      {/* {props.error && <div className="alert alert-danger">{props.error}</div>} */}
    </div>
  );
};

export default Input;
