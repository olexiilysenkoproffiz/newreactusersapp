import React, { Component } from "react";
import { Link } from "react-router-dom";


import { connect } from 'react-redux';
import { deleteUser, createUserAsync, setPropertyValue, getUsers } from '../../store/actions/UsersActions';
import { toggleProperty } from '../../store/actions/PropertyActions';

import { Modals } from '../../services/ModalService';
import SignUpModal from "../SignUpModal";
import LoginModal from "../LoginModal";
import ModalComponent from "../ModalComponent";
import VerificationModal from "../VerificationModal";
import CongratulationModal from "../CongratulationModal";
import ForgotPasswordModal from "../ForgotPasswordModal";
import ResetPasswordModal from "../ResetPasswordModal";
import EmailSentModal from "../EmailSentModal";
import UsersTable from "../table/UsersTable";
import {
  signUp,
  forgotPassword,
  loginUser,
  verifyUser
} from "../../services/AuthService";
import { updateUser } from "../../services/UserService";
import { BASE_URL } from "../../constants/environment";
import { validateEmail, isEmpty } from "../../utils/StringUtil";

import axios from "axios";

import "./Dashboard.css";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    // configure baseURL for different environments
    this.baseUrl = BASE_URL;

  }
  async componentDidMount() {
    this.props.getUsers();
  }

  // update Redux Store property with new value 
  updateStoreProperty = (propertyName, newValue) => {
    const changedValue = {
      property: propertyName,
      value: newValue
    };
    this.props.setPropertyValue(changedValue);
  }

  // CREATE new user
  handleFormSubmit = async e => {
    e.preventDefault();
    let userData = this.props.newUser;
    this.props.createUser(userData);
    this.handleToggle();
    this.handleClearForm();
  };

  handleClearForm = e => {
    const newUserObj = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      isAdmin: false
    }
    // optimized version
    this.updateStoreProperty('newUser', newUserObj);
    this.updateStoreProperty('errors', {});
  };

  // handle all inputs in modals
  handleInput = e => {
    const errors = { ...this.props.errors };
    const errorMessage = this.validateProperty(e.target);

    if (errorMessage) errors[e.target.name] = errorMessage;
    else delete errors[e.target.name];

    let value = e.target.value;
    let name = e.target.name;
    const newUserObj = {
      ...this.props.newUser,
      [name]: value
    }

    this.updateStoreProperty('newUser', newUserObj);
    this.updateStoreProperty('errors', errors);
    console.log(name, value);
  };

  // UPDATE user
  handleUpdate = async (e, user) => {
    e.preventDefault();
    const payload = this.props.newUser;
    await updateUser(payload).then(
      response => {
        console.log("response from patch: ", response);
      },
      err => {
        console.log(err);
      }
    );

    // changing the view according to database
    const users = [...this.props.users];
    //find the right index of edited user
    for (let i = 0; i < users.length; i++) {
      //search for id matched
      if (users[i]._id === this.props.newUser._id) {
        users[i] = { ...this.props.newUser };
      } else {
        continue;
      }
    }
    console.log("users from handleUpdate: ", users);
    this.updateStoreProperty('users', users);
    this.props.toggleProperty('modalEdit');
    this.handleClearForm();
  };

  // delete user from db
  handleDelete = async user => {
    if (
      JSON.stringify(localStorage.getItem("token")).length > 30 &&
      this.props.isLoggedIn &&
      this.props.isAdmin
    ) {
      let token = localStorage.getItem("token");
      const userData = {
        user,
        token
      }
      this.props.deleteUser(userData);
      const users = this.props.users.filter(u => u._id !== user._id);
      this.updateStoreProperty('users', users);
    } else if (this.props.isAdmin === false) {
      alert(
        "Access denied! You should to be Logged in as Admin to be able Delete the user."
      );
    }
  };

  handleToggle = () => {
    this.props.toggleProperty(Modals.modal);
  };

  handleToggleEdit = async user => {
    if (this.props.isLoggedIn === true) {
      this.props.toggleProperty(Modals.modalEdit);

      const users = [...this.props.users];
      const index = users.indexOf(user);
      users[index] = { ...user };
      console.log(`users[index] : `, users[index]);
      this.updateStoreProperty('newUser', users[index])
    } else {
      alert(
        "Access denied! You should be Logged in to be able to Edit the user."
      );
    }
  };

  handleToggleSignUp = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalSignUp);
  };

  handleToggleLogin = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalLogin);
  };

  handleToggleVerify = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalVerify);
    if (localStorage.getItem('beer') !== 'false') {
      this.props.toggleProperty('isVerified');
    }
    console.log('isVerified changed to:', this.props.isVerified);
    localStorage.setItem("beer", "false");
  };

  handleToggleCongratulations = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalCongratulations);
  };

  handleToggleForgot = () => {
    if (this.props.modalLogin) {
      this.handleToggleLogin();
    }
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalForgot);
  };

  handleToggleReset = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalReset);
  };

  handleToggleEmailSent = () => {
    this.handleClearForm();
    this.props.toggleProperty(Modals.modalEmailSent);
  };

  // register the user and login immediatly
  handleSignUp = async e => {
    e.preventDefault();
    // assign the Authorization header to all axios requests by default
    axios.defaults.headers.common["Authorization"] =
      "Bearer " + localStorage.getItem("token");
    // send request to the back end with signing up info
    const obj = {
      password: this.props.newUser.password,
      email: this.props.newUser.email
    };

    localStorage.setItem('newUserEmail', obj.email);

    // validation
    const errors = this.validate(); // returns an error-like object
    this.updateStoreProperty('errors', errors || {});
    if (errors) return;

    try {
      const response = await signUp(obj); //axios returns token and bearer + verification secretToken
      console.log("response from signup function: ", response);
      this.handleToggleSignUp();
      this.handleToggleVerify();
    } catch (ex) {
      if (
        (ex.response && ex.response.status === 400) ||
        (ex.response && ex.response.status === 401) ||
        (ex.response && ex.response.status === 409)
      ) {
        const emailExists = {
          email: "The email you entered already exists."
        };
        this.updateStoreProperty('errors', emailExists);
      } else if (ex.response && ex.response.status === 500) {
        const emailPattern = {
          password:
            "We`re sorry, but something going wrong on the server! Please, try again later."
        };
        this.updateStoreProperty('errors', emailPattern);
      } else {
        this.handleToggleSignUp();
        alert(
          "We`re sorry, but something going wrong! Please, try again later."
        );
      }
    }
  };

  // validation
  validate = () => {
    const errors = {};
    const { email, password } = this.props.newUser;

    let emailTested = validateEmail(email);
    if (!emailTested)
      errors.email = "Please, enter the valid email: example@gmail.com";

    if (isEmpty(email)) errors.email = "Email is required.";
    if (isEmpty(password)) errors.password = "Password is required.";


    return Object.keys(errors).length === 0 ? null : errors;
  };

  handleCongratulations = async e => {
    e.preventDefault();
    this.handleToggleCongratulations();
    this.handleToggleLogin();
  };

  handleEmailSent = async e => {
    e.preventDefault();
    this.handleToggleEmailSent();
  };

  handleReset = async e => {
    e.preventDefault();
    console.log("HANDLE RESET works!");
  };

  handleForgot = async e => {
    e.preventDefault();
    let errors = {};

    const enteredEmail = {
      email: this.props.newUser.email
    };

    if (enteredEmail.email === undefined) {
      return;
    }
    // send the email to the back-end
    await forgotPassword(enteredEmail)
      .then((res, err) => {
        console.log("response from db: ", res);
        if (res.status === 401 || res.status === 501 || res.status === 404) {
          errors.email = "The email you`ve entered doesn`t exist.";
          console.log("wrong email entered");
          const forgotError = {
            property: 'errors',
            value: errors
          };
          this.props.setPropertyValue(forgotError);
          if (errors) return;
        }
        if (res.status === 200) {
          console.log(
            "user refresh password link has been sent to the entered email!"
          );
          this.handleClearForm();
          this.handleToggleForgot();
          this.handleToggleEmailSent();
        }
      })
      .catch(err => {
        console.log("the error from handleForgot: ", err);
        if (err.response.status === 500) {
          errors.secretToken =
            "We`re sorry, something going wrong on server. Please try again later.";
          const forgotError = {
            property: 'errors',
            value: errors
          };
          this.props.setPropertyValue(forgotError);
        }
        if (err.response.status === 501) {
          errors.email =
            "The email you`ve entered is wrong, please enter the valid email";
          const forgotError = {
            property: 'errors',
            value: errors
          };
          this.props.setPropertyValue(forgotError);
        }
        if (err.response.status === 404) {
          errors.email =
            "The email you`ve entered is wrong, please enter the valid email";
          const forgotError = {
            property: 'errors',
            value: errors
          };
          this.props.setPropertyValue(forgotError);
        }

        console.log("wrong email entered");
        const forgotError = {
          property: 'errors',
          value: errors
        };
        this.props.setPropertyValue(forgotError);
        if (errors) return;
      });
  };

  handleVerify = async e => {
    e.preventDefault();
    let errors = {};
    // compare the entered verification code to the one
    // that cames from the back-end
    const enteredVerificationCode = {
      secretToken: this.props.newUser.secretToken
    };
    console.log("verification code entered: ", enteredVerificationCode);
    // find one user which secretToken equals to the entered token:
    console.log("enteredVerificationCode is: ", enteredVerificationCode);

    if (enteredVerificationCode.secretToken !== undefined) {
      const verificationCodeFromDb = await verifyUser(enteredVerificationCode)
        .then((res, err) => {
          console.log("response from db: ", res);
          if (res.status === 401 || res.status === 501) {
            errors.secretToken =
              "The secretToken you`ve entered is wrong, please enter the valid secretToken";
            this.updateStoreProperty('errors', errors);
            if (errors) return;
          }
          if (res.status === 201) {
            console.log("the user successfully verified! Now can logIn !");
            this.handleClearForm();
            this.handleToggleVerify();
            this.handleToggleCongratulations();
            localStorage.setItem("beer", "true");
          }
        })
        .catch(err => {
          console.log("the error from handleVerify: ", err);
          if (err.response.status === 500) {
            errors.secretToken =
              "We`re sorry, something going wrong on server. Please try again later.";
            const verifyError = {
              property: 'errors',
              value: errors
            };
            this.props.setPropertyValue(verifyError);
          }
          if (err.response.status === 501) {
            errors.secretToken =
              "The secretToken you`ve entered is wrong, please enter the valid secretToken";
            const verifyError = {
              property: 'errors',
              value: errors
            };
            this.props.setPropertyValue(verifyError);
          }

          console.log("wrong secretToken entered");
          const verifyError = {
            property: 'errors',
            value: errors
          };
          this.props.setPropertyValue(verifyError);
          if (errors) return;
        });
      console.log("verification token has been sent to the server");
      console.log("verification response from db:", verificationCodeFromDb);
    } else {
      errors.secretToken =
        "Please, enter the verification secretToken from received email.";
      const verifyError = {
        property: 'errors',
        value: errors
      };
      this.props.setPropertyValue(verifyError);
    }
  };

  handleLogin = async e => {
    e.preventDefault();
    const obj = {
      password: this.props.newUser.password,
      email: this.props.newUser.email
    };
    // validation
    const errors = this.validate();

    const loginError = {
      property: 'errors',
      value: errors
    }
    this.props.setPropertyValue(loginError);
    if (errors) return;

    try {
      // sending request to the back-end with info to compare
      // receive back the token

      const { data: jwt } = await loginUser(obj); // axios returns or not a token
      // save the token to localStorage
      localStorage.setItem("token", jwt.token);
      console.log("response from login form: ", jwt);
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("token");

      const isLoggedIn = {
        property: 'isLoggedIn',
        value: true
      }
      const isAdmin = {
        property: 'isAdmin',
        value: jwt.isAdmin
      }

      this.props.setPropertyValue(isLoggedIn);
      this.props.setPropertyValue(isAdmin);

      // set the isLoggedIn to localStorage to prevent the erasing 
      // of info when accidentally refreshing the page
      localStorage.setItem("cocaCola", JSON.stringify(isLoggedIn.value));
      if (isAdmin) {
        localStorage.setItem("sprite", JSON.stringify(isAdmin.value));
      }
      // set the logged in username and email to the localStorage
      let email = localStorage.getItem("email")
        ? localStorage.getItem("email")
        : obj.email;

      const loggedUser = {
        property: 'loogedUser',
        value: email
      }
      this.props.setPropertyValue(loggedUser);
      localStorage.setItem("email", obj.email);
      this.handleToggleLogin();
    } catch (ex) {
      if (
        (ex.response && ex.response.status === 400) ||
        (ex.response && ex.response.status === 501)
      ) {
        const wrongLogin = {
          password: "Current combination of email and password is incorrect"
        };
        const logErrors = {
          property: 'errors',
          value: wrongLogin
        }
        this.props.setPropertyValue(logErrors)
      } else if (ex.response && ex.response.status === 401) {
        const notVerified = {
          email: "Please, verify your email first, then log in."
        };
        const logErrors = {
          property: 'errors',
          value: notVerified
        }
        this.props.setPropertyValue(logErrors)
        this.handleToggleLogin();
        setTimeout(this.handleToggleVerify(), 2000);
      } else {
        const serverError = {
          password:
            "We`re sorry, but something going wrong on the server! Please, try again later."
        };
        const logErrors = {
          property: 'errors',
          value: serverError
        }
        this.props.setPropertyValue(logErrors);
      }
    }
  };

  // handle logOut
  handleLogOut = e => {
    e.preventDefault();
    localStorage.removeItem("token");
    console.log("logged user object: ", this.props.newUser);

    this.props.toggleProperty("isLoggedIn");

    localStorage.removeItem("cocaCola");
    localStorage.removeItem("sprite");
    localStorage.removeItem("beer");
    localStorage.removeItem("email");
    localStorage.removeItem("newUserEmail");
    window.location.reload();
  };

  validateProperty = input => {
    if (input.name === "email") {
      console.log('input', input.value);
      if (isEmpty(input.value)) return "Email is required.";
    }
    if (input.name === "password") {
      if (isEmpty(input.value)) return "Password is required.";
    }
  };

  render() {
    console.log(this.props);
    const { users, loggedUser, modal, modalEdit, modalForgot, modalReset,
      modalEmailSent,
      modalSignUp,
      modalLogin,
      modalVerify,
      modalCongratulations,
      isVerified,
      isAdmin,
      isLoggedIn,
      // password,
      newUser,
      errors
    } = this.props;

    const showMe = { display: "block" };
    const hideMe = { display: "none" };
    // const { isLoggedIn } = this.props;

    return (
      <div className="App">
        <h1>Users</h1>
        {isLoggedIn ? (
          <div>
            <div
              className="btn-group btn-toolbar justify-content-end col-md-11 ml-2 mb-3"
              role="group"
              aria-label="Basic example"
            >
              <button
                type="button"
                className="btn btn-secondary"
                onClick={this.handleLogOut}
              >
                Log out
              </button>
            </div>

            <div className="loggedIn">
              Welcome,
              <Link to={`/` + this.props.loggedUser} style={usercolor}>
                {loggedUser}
              </Link>
            </div>
          </div>
        ) : (
            <div>
              {!isVerified && (
                <div className="alert alert-danger">
                  Your email is not verified. Please, verify your email before
                logging in:{" "}
                  <button onClick={this.handleToggleVerify}>Verify!</button>
                </div>
              )}
              <div
                className="btn-group btn-toolbar justify-content-end col-md-11 ml-2 mb-3"
                role="group"
                aria-label="Basic example"
              >
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={() => {
                    this.handleToggleLogin();
                  }}
                >
                  Login
              </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={() => {
                    this.handleToggleSignUp();
                  }}
                >
                  SignUp
              </button>
              </div>
            </div>
          )}

        <div>
          <UsersTable
            // table of users
            isLoggedIn={isLoggedIn}
            isAdmin={isAdmin}
            onDelete={user => {
              this.handleDelete(user);
            }}
            onToggleEdit={user => {
              this.handleToggleEdit(user);
            }}
            users={users}
          />

          <button
            onClick={() => {
              this.handleToggle();
            }}
            className="btn btn-success"
          >
            Add New User
          </button>

          {!isLoggedIn && (
            <div>
              <SignUpModal
                // on SignUp
                onToggle={this.handleToggleSignUp}
                modal={modalSignUp}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleSignUp}
                modalHeader="Sign up"
                show={hideMe}
                error={errors}
              />

              <LoginModal
                //on Login
                onToggle={this.handleToggleLogin}
                modal={modalLogin}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleLogin}
                modalHeader="Log in"
                show={hideMe}
                error={errors}
                forgot={this.handleToggleForgot}
              />

              <VerificationModal
                // Verification modal
                onToggle={this.handleToggleVerify}
                modal={modalVerify}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleVerify}
                modalHeader="Verify your email:"
                show={hideMe}
                error={errors}
              />

              <CongratulationModal
                // Congratulation modal
                onToggle={this.handleToggleCongratulations}
                modal={modalCongratulations}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleCongratulations}
                modalHeader="Congratulations!"
                show={hideMe}
              />

              <ForgotPasswordModal
                // Forgot Password modal
                onToggle={this.handleToggleForgot}
                modal={modalForgot}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleForgot}
                modalHeader="Send refresh link:"
                show={hideMe}
                error={errors}
              />

              <ResetPasswordModal
                // Reset password modal
                onToggle={this.handleToggleReset}
                modal={modalReset}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleReset}
                modalHeader="Reset your password:"
                show={hideMe}
                error={errors}
              />

              <EmailSentModal
                // Email sent modal
                onToggle={this.handleToggleEmailSent}
                modal={modalEmailSent}
                newUser={newUser}
                onInput={this.handleInput}
                onFormSubmit={this.handleEmailSent}
                modalHeader="Email Sent Successfuly!"
                show={hideMe}
                error={errors}
              />
            </div>
          )}

          <ModalComponent
            // add new User to table
            onToggle={this.handleToggle}
            modal={modal}
            newUser={newUser}
            onInput={this.handleInput}
            onFormSubmit={this.handleFormSubmit}
            modalHeader="New User"
            buttonTitle="Add"
            show={showMe}
          />

          <ModalComponent
            // edit user
            onToggle={this.handleToggleEdit}
            modal={modalEdit}
            newUser={newUser}
            onInput={this.handleInput}
            onFormSubmit={this.handleUpdate}
            modalHeader="Edit User"
            buttonTitle="Edit"
            show={hideMe}
          />
        </div>
      </div>
    );
  }
}



const usercolor = {
  color: "lightblue"
};

const mapStateToProps = (state) => {
  return {
    users: state.user.users,
    loggedUser: localStorage.getItem("email"),
    modal: state.user.modal,
    modalEdit: state.user.modalEdit,
    modalForgot: state.user.modalForgot,
    modalReset: state.user.modalReset,
    modalEmailSent: state.user.modalEmailSent,
    modalSignUp: state.user.modalSignUp,
    modalLogin: state.user.modalLogin,
    modalVerify: state.user.modalVerify,
    modalCongratulations: state.user.modalCongratulations,
    isVerified: state.user.isVerified,
    isAdmin:
      state.user.isAdmin,
    isLoggedIn:
      state.user.isLoggedIn,
    password: state.user.password,
    newUser: state.user.newUser,
    errors: state.user.errors
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteUser: (user) => { dispatch(deleteUser(user)) },
    createUser: (newUser) => { dispatch(createUserAsync(newUser)) },
    toggleProperty: (propertyName) => { dispatch(toggleProperty(propertyName)) },
    setPropertyValue: (payload) => { dispatch(setPropertyValue(payload)) },
    getUsers: () => { dispatch(getUsers()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
