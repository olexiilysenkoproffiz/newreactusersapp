import React, { Component } from 'react';
import { Link } from "react-router-dom";


import { connect } from 'react-redux';
import { setPropertyValue } from '../../store/actions/UsersActions';
import { editProfilePassword, editProfileImage, editProfileInfo, editProfileEmail } from '../../store/actions/ProfileActions';
import "./UserProfile.css";

import { BASE_URL } from "../../constants/environment";
import { getUserDetails } from '../../services/UserService';




class UserProfile extends Component {
    constructor(props) {
        super(props);
        // configure baseURL for different environments
        this.baseUrl = BASE_URL;

    }

    async componentDidMount() {
        getUserDetails(this.props.loggedUser).then((user) => {
            console.log('user details: ', user);

            // set the properties of the User
            const userProfileData = {
                bio: (user.data.data && user.data.data.bio) || this.props.loggedUserProfile.bio,
                interests: (user.data.data && user.data.data.interests) || this.props.loggedUserProfile.interests,
                experience: (user.data.data && user.data.data.experience) || this.props.loggedUserProfile.experience,
                profile: (user.data.data && user.data.data.profile) || this.props.loggedUserProfile.profile,
                location: (user.data.data && user.data.data.location) || this.props.loggedUserProfile.location,
                experienceYears: (user.data.data && user.data.data.experienceYears) || this.props.loggedUserProfile.experienceYears,
                firstname: (user.data.data && user.data.data.firstname) || this.props.loggedUserProfile.firstname,
                lastname: (user.data.data && user.data.data.lastname) || this.props.loggedUserProfile.lastname,
                email: (user.data.data && user.data.data.email) || this.props.loggedUserProfile.email,
                password: (user.data.data && user.data.data.password) || this.props.loggedUserProfile.password,
                image: (user.data.data && user.data.data.image) || this.props.loggedUserProfile.image, // path to the image in uploads folder;
                cv: (user.data.data && user.data.data.cv) || this.props.loggedUserProfile.cv
            }

            // update properties
            this.updateStoreProperty('loggedUserProfile', userProfileData);
        });
    }

    // update Redux Store property with new value 
    updateStoreProperty = (propertyName, newValue) => {
        const changedValue = {
            property: propertyName,
            value: newValue
        };
        this.props.setPropertyValue(changedValue);
    }

    render() {

        const {
            bio,
            interests,
            experience,
            experienceYears,
            location,
            profile,
            firstname,
            lastname,
            image,
            cv,
            email,
            password
        } = this.props.loggedUserProfile;

        const str1 = "url('";
        const str2 = image;
        const res = str1.concat(str2);
        const str3 = "')";
        const url = res.concat(str3);

        let userProfileImage = {
            backgroundImage: `${url}`,
        }
        userProfileImage.backgroundImage = url;

        const loggedUser = this.props.loggedUser;

        const userFirstLastname = firstname + ` ` + lastname;
        const welcomeMessage = firstname ? `Dear ` + firstname + `, please, fill up the additional info to let us know more about you.` : `Dear Friend, please, fill up the additional info to let us know more about you.`
        return (
            <div className="profileBackground">
                <div style={profileBody}>
                    <h1>
                        Profile
                    </h1>
                    <p>{welcomeMessage}</p>
                </div>
                <div style={profileCard}>
                    <div style={profileCardHeader} className='profileCardHeader'>
                        <div className="userProfileImage" style={userProfileImage}></div>
                        <div className="profile-username-info">
                            <h4 className="profileUsername">{userFirstLastname}</h4>
                            <h6 className="developer-direction">{profile}</h6>
                        </div>
                        <h6 className="location"><i className="fas fa-map-marker-alt" style={iconsStyle}></i>{location}</h6>
                        <h6 className="experience"><i className="fas fa-hammer" style={iconsStyle}></i>Experience: <b>{experienceYears} years</b></h6>

                        <button className="btn btn-default download"><i className="fas fa-file-download" style={iconsStyle}></i><span className="download-button-text">  Download CV</span></button>
                    </div>

                    <div className="profileCardBody">
                        <div className="column-left">
                            <div className="column-left-profile-navigation"><Link to={`/` + loggedUser} style={{ textDecoration: 'none' }}>Profile</Link></div>
                            <div className="column-left-profile-navigation"><Link to={`/` + loggedUser + `/edit`} style={{ textDecoration: 'none' }}>Edit</Link></div>
                            <div className="column-left-profile-navigation"><Link to="/" style={{ textDecoration: 'none' }}>Main</Link></div>
                        </div>
                        <div className="profile-column-right">
                            <div className="column-right-row">
                                <div className="column-right-lable">Bio</div>
                                <div className="column-right-info">{bio}</div>
                            </div>
                            <div className="column-right-row">
                                <div className="column-right-lable">Experience</div>
                                <div className="column-right-info">{experience}</div>
                            </div>
                            <div className="column-right-row">
                                <div className="column-right-lable">Interests</div>
                                <div className="column-right-info">{interests}</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}



const profileBody = {
    textAlign: 'center',
    paddingTop: '20px',
};

const profileCard = {
    margin: '0px auto',
    backgroundColor: 'white',
    width: '1200px',
    height: '1000px',
    borderRadius: '5px',
};

const profileCardHeader = {
    height: '120px',
    backgroundColor: 'lightgrey'
}

const iconsStyle = {
    paddingRight: '7px'
}

const mapStateToProps = (state) => {
    return {
        // only needed for this component functionality properties
        loggedUser: localStorage.getItem("email"),
        isAdmin:
            state.user.isAdmin,
        isLoggedIn:
            state.user.isLoggedIn,
        loggedUserProfile: state.user.loggedUserProfile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setPropertyValue: (payload) => { dispatch(setPropertyValue(payload)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);