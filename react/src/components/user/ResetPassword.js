import React, { Component } from "react";
import ResetPasswordModal from "../ResetPasswordModal";
import axios from "axios";

class ResetPassword extends Component {
  state = {
    modalReset: true,
    newUser: {
      firstname: "",
      lastname: "",
      email: "",
      _id: "",
      password: "",
      passwordConfirm: "",
      isAdmin: false,
      isLoggedIn: false,
      active: false,
      secretToken: ""
    },
    errors: {}
  };
  componentDidMount = () => {
    console.log("params from the browser bar: ", this.props.match.params.id);
  };

  handleToggleReset = () => {
    console.log("Hello from Reset modal");
    let modalReset = !this.state.modalReset;
    this.handleClearForm();

    this.setState({
      modalReset
    });
  };

  handleReset = async e => {
    // handle code
    e.preventDefault();
    let errors = {};
    console.log("password", this.state.newUser.password);
    console.log("passwordConfirm", this.state.newUser.passwordConfirm);
    // take the entered password
    const password = this.state.newUser.password;
    const passwordConfirm = this.state.newUser.passwordConfirm;

    if (password === passwordConfirm) {
      console.log("passwords matches!");

      // validation
      let errors = this.validate();
      this.setState({ errors: errors || {} });
      if (errors) return;

      // make obj to send it to the server
      const obj = {
        password: this.state.newUser.password,
        _id: this.props.match.params.id,
        token: this.props.match.params.token
      };

      console.log("maked object for request: ", obj);

      // post entered passwords to the backend endpoint
      await axios.post("/api/userAuths/resetpassword", obj);
      // save it there with the same user (find by id)
      this.handleToggleReset();
      // redirect to the Home page
      this.props.history.push("/");
    } else {
      errors.password = "Sorry, entered passwords not match!";
      this.setState({ errors: errors });
      if (errors) return;
    }
  };

  handleClearForm = e => {
    // e.preventDefault();
    this.setState({
      newUser: {
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        passwordConfirm: "",
        isAdmin: false
      },
      errors: {}
    });
    console.log("from the handleClearForm method");
  };

  // handle all inputs in modals
  handleInput = e => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(e.target);

    if (errorMessage) errors[e.target.name] = errorMessage;
    else delete errors[e.target.name];

    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => {
        return {
          newUser: {
            ...prevState.newUser,
            [name]: value
          },
          errors
        };
      },
      () => console.log(this.state.newUser)
    );
  };

  validateProperty = input => {
    if (input.name === "email") {
      if (input.value.trim() === "") return "Email is required.";
    }
    if (input.name === "password") {
      if (input.value.trim() === "") return "Password is required.";
    }
  };

  // validation
  validate = () => {
    const errors = {};

    // use object destructuring
    const { passwordConfirm, password } = this.state.newUser;

    if (password.trim() === "") errors.password = "Password is required.";
    if (passwordConfirm.trim() === "")
      errors.passwordConfirm = "Confirm Password field is required.";
    return Object.keys(errors).length === 0 ? null : errors;
  };

  render() {
    const showMe = { display: "block" };
    // const hideMe = { display: "none" };
    // const { isLoggedIn } = this.state;
    return (
      <div>
        {/* <h2>{this.props.match.params.id}</h2> */}
        {/* <h2>Congrats! Now you can login with new password!</h2>
        <h3>To return back to the app, please click: </h3>
        <div>
          <button>Return to the App</button>
        </div> */}

        <ResetPasswordModal
          // Verification modal
          onToggle={this.handleToggleReset}
          modal={this.state.modalReset}
          newUser={this.state.newUser}
          onInput={this.handleInput}
          onFormSubmit={this.handleReset}
          modalHeader="Reset your password:"
          show={showMe}
          error={this.state.errors}
        />
      </div>
    );
  }
}

export default ResetPassword;
