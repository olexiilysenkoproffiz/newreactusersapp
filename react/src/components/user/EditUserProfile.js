import React, { Component } from 'react';
import { Link } from "react-router-dom";


import { connect } from 'react-redux';
import { setPropertyValue } from '../../store/actions/UsersActions';
import { editProfilePassword, editProfileImage, editProfileInfo, editProfileEmail } from '../../store/actions/ProfileActions';
import "./EditUserProfile.css";

import { BASE_URL, BASE_URL1 } from "../../constants/environment";
import { getUserDetails } from '../../services/UserService';




class UserProfile extends Component {
    constructor(props) {
        super(props);
        // configure baseURL for different environments
        this.baseUrl = BASE_URL1;

    }

    async componentDidMount() {
        getUserDetails(this.props.loggedUser).then((user) => {
            console.log('user details: ', user);
            if (!user.data.data) {
                this.props.history.push('/');
                return;
            } else {
                // set the properties of the User
                const userProfileData = {
                    bio: (user.data.data.bio) || this.props.loggedUserProfile.bio || "no info",
                    interests: (user.data.data.interests) || this.props.loggedUserProfile.interests || "no info",
                    experience: (user.data.data.experience) || this.props.loggedUserProfile.experience || "no info",
                    profile: (user.data.data.profile) || this.props.loggedUserProfile.profile || "no info",
                    location: (user.data.data.location) || this.props.loggedUserProfile.location || "no info",
                    experienceYears: (user.data.data.experienceYears) || this.props.loggedUserProfile.experienceYears || "no info",
                    firstname: (user.data.data.firstname) || this.props.loggedUserProfile.firstname || "no info",
                    lastname: (user.data.data.lastname) || this.props.loggedUserProfile.lastname || "no info",
                    email: (user.data.data.email) || this.props.loggedUserProfile.email,
                    password: (user.data.data.password) || this.props.loggedUserProfile.password || "no info",
                    image: (user.data.data.image) || this.props.loggedUserProfile.image || "no info", // path to the image in uploads folder;
                    cv: (user.data.data.cv) || this.props.loggedUserProfile.cv || "no info"
                }
                // update properties in Redux Store
                this.updateStoreProperty('loggedUserProfile', userProfileData);

            }
        });

    }

    // update Redux Store property with new value 
    updateStoreProperty = (propertyName, newValue) => {
        const changedValue = {
            property: propertyName,
            value: newValue
        };
        this.props.setPropertyValue(changedValue);
    }

    handleInput = e => {
        let value = e.target.value;
        let name = e.target.name;
        const newUserObj = {
            ...this.props.loggedUserProfile,
            [name]: value
        }

        this.updateStoreProperty('loggedUserProfile', newUserObj);
        // this.updateStoreProperty('errors', errors);
        console.log(name, value);
        // }
    };

    handleSelectedFile = (e) => {
        console.log('image selected', e.target.files);
        let value = e.target.files[0];
        let name = e.target.name;
        const newUserObj = {
            ...this.props.loggedUserProfile,
            [name]: value
        }

        this.updateStoreProperty('loggedUserProfile', newUserObj);
    }

    // handleSelectedCV = (e) => {
    //     console.log('CV selected', e.target.files);
    //     let value = e.target.files[0];
    //     let name = e.target.name;
    //     const newUserObj = {
    //         ...this.props.loggedUserProfile,
    //         [name]: value
    //     }
    //     this.updateStoreProperty('loggedUserProfile', newUserObj);
    // }

    handleSaveUserProfile = (e) => {
        e.preventDefault();
        console.log('props ===>', this.props);
        // add files to data object
        const data = new FormData();
        data.append('bio', this.props.loggedUserProfile.bio);
        data.append('interests', this.props.loggedUserProfile.interests);
        data.append('experience', this.props.loggedUserProfile.experience);
        data.append('profile', this.props.loggedUserProfile.profile);
        data.append('location', this.props.loggedUserProfile.location);
        data.append('experienceYears', this.props.loggedUserProfile.experienceYears);
        data.append('firstname', this.props.loggedUserProfile.firstname);
        data.append('lastname', this.props.loggedUserProfile.lastname);
        data.append('email', this.props.loggedUserProfile.email);
        data.append('password', this.props.loggedUserProfile.password);
        data.append('image', this.props.loggedUserProfile.image);
        data.append('cv', this.props.loggedUserProfile.cv);
        // send new data to API
        this.props.editProfileInfo(data);
    };


    render() {
        const {
            bio,
            interests,
            experience,
            experienceYears,
            location,
            profile,
            firstname,
            lastname,
            image,
            cv,
            email,
            password
        } = this.props.loggedUserProfile;

        // profile image url path
        const str1 = "url('";
        // TODO: after DEPLOY need to change the baseUrl;
        const str2 = this.baseUrl + image;
        const res = str1.concat(str2);
        const str3 = "')";
        const url = res.concat(str3);

        let userProfileImage = {
            backgroundImage: `${url}`,
        }

        // download CV url path
        const downloadCvUrl = this.baseUrl + cv;

        const userFirstLastname = firstname + ` ` + lastname;
        const loggedUser = this.props.loggedUser;
        const welcomeMessage = firstname ? `Dear ` + firstname + `, please, fill up the additional info to let us know more about you.` : `Dear Friend, please, fill up the additional info to let us know more about you.`

        return (
            <div className="profileBackground">
                <div style={profileBody}>
                    <h1>
                        Edit Profile
                    </h1>
                    <p>{welcomeMessage}</p>
                </div>
                <div style={profileCard}>
                    <div style={profileCardHeader} className='profileCardHeader'>
                        <div className="userProfileImage" style={userProfileImage}></div>
                        <div className="profile-username-info">
                            <h4 className="profileUsername">{userFirstLastname}</h4>
                            <h6 className="developer-direction">{profile}</h6>
                        </div>
                        <h6 className="location location-edit"><i className="fas fa-map-marker-alt" style={iconsStyle}></i>{location}</h6>
                        <h6 className="experience experience-edit"><i className="fas fa-hammer" style={iconsStyle}></i>Experience: <b>{experienceYears} years</b></h6>
                        <a className="btn btn-default download" role="button" href={downloadCvUrl}
                            download={cv}><i className="fas fa-file-download" style={iconsStyle}></i><span className="download-button-text">
                                Download CV</span>
                        </a>
                        {/* <button className="btn btn-default download"><i className="fas fa-file-download" style={iconsStyle}></i><span className="download-button-text">  Download CV</span></button> */}
                    </div>

                    <div className="profileCardBody">
                        <div className="column-left edit-column-left">
                            <div className="column-left-profile-navigation"><Link to={`/` + loggedUser} style={{ textDecoration: 'none' }}>Profile</Link></div>
                            <div className="column-left-profile-navigation"><Link to={`/` + loggedUser + `/edit`} style={{ textDecoration: 'none' }}>Edit</Link></div>
                            <div className="column-left-profile-navigation"><Link to="/" style={{ textDecoration: 'none' }}>Main</Link></div>
                        </div>
                        <form action="/:userEmail" method="post" encType="multipart/form-data" className="column-right" onSubmit={this.handleSaveUserProfile}>
                            <div className="column-right-info-block">

                                <div className="column-right-row">
                                    <div className="column-right-lable">Firstname: </div>
                                    <div className="column-right-info">
                                        <input style={{ width: '150px' }} name="firstname" id="firstname" value={firstname} onChange={this.handleInput}></input>
                                    </div>
                                </div>
                                <div className="column-right-row">
                                    <div className="column-right-lable">Lastname: </div>
                                    <div className="column-right-info">
                                        <input style={{ width: '150px' }} name="lastname" id="lastname" value={lastname} onChange={this.handleInput}></input>
                                    </div>
                                </div>
                                <div className="column-right-row">
                                    <div className="column-right-lable">Exp.years: </div>
                                    <div className="column-right-info">
                                        <input style={{ width: '150px' }} name="experienceYears" id="experienceYears" value={experienceYears || ""} onChange={this.handleInput}></input>
                                    </div>
                                </div>
                                <div className="column-right-row">
                                    <div className="column-right-lable">Location: </div>
                                    <div className="column-right-info">
                                        <input style={{ width: '150px' }} name="location" id="location" value={location} onChange={this.handleInput}></input>
                                    </div>
                                </div>
                                <div className="column-right-row">
                                    <div className="column-right-lable">Speciality: </div>
                                    <div className="column-right-info">
                                        <input style={{ width: '150px' }} name="profile" id="profile" value={profile} onChange={this.handleInput}></input>
                                    </div>
                                </div>
                            </div>

                            <div className="column-right-row">
                                <div className="column-right-lable">Bio</div>
                                <div className="column-right-info">
                                    <textarea name="bio" id="biography" cols="115" rows="5" value={bio} onChange={this.handleInput}></textarea>
                                </div>
                            </div>
                            <div className="column-right-row">
                                <div className="column-right-lable">Experience</div>
                                <div className="column-right-info">
                                    <textarea name="experience" id="experience" cols="115" rows="5" value={experience} onChange={this.handleInput}></textarea>
                                </div>
                            </div>
                            <div className="column-right-row">
                                <div className="column-right-lable">Interests</div>
                                <div className="column-right-info">
                                    <textarea name="interests" id="interests" cols="115" rows="5" value={interests} onChange={this.handleInput}></textarea>
                                </div>
                            </div>


                            <div className="column-right-credentials-block">

                                <div className="column-right-row upload">
                                    <div className="column-right-lable">Upload avatar/CV: </div>
                                    <input type="file" style={{ width: '210px' }} name="image" id="image" onChange={this.handleSelectedFile}></input>
                                    <input type="file" style={{ width: '210px', marginTop: '10px' }} name="cv" id="cv" onChange={this.handleSelectedFile}></input>
                                </div>
                                <div className="column-right-row" style={{ border: '1px dotted lightgrey', padding: '10px', background: '#d0ede2' }}>
                                    <div className="column-right-lable">Change e-mail: </div>
                                    <div className="column-right-info password">
                                        <input style={{ width: '180px' }} name="email" id="email" placeholder={loggedUser} onChange={this.handleInput}></input>
                                        <input style={{ width: '180px', marginTop: '10px' }} name="confirm_email" id="confirm_email" placeholder="Confirm e-mail" onChange={this.handleInput}></input>
                                    </div>
                                </div>
                                <div className="column-right-row" style={{ marginLeft: '20px', border: '1px dotted lightgrey', padding: '10px', background: '#d0ede2' }}>
                                    <div className="column-right-lable">Change Password: </div>
                                    <div className="column-right-info password">
                                        <input style={{ width: '180px' }} type="password" name="password" id="password" placeholder="New password" onChange={this.handleInput}></input>
                                        <input style={{ width: '180px', marginTop: '10px' }} type="password" name="confirm_password" id="confirm_password" placeholder="Confirm password" onChange={this.handleInput}></input>
                                    </div>
                                </div>
                            </div>

                            <div className="profileCardFooter">
                                <button className="btn btn-info save" type="submit">Save</button>
                                <Link to={`/` + loggedUser} style={cancelButton} className="cancel">Cancel</Link>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        );
    }
}

const cancelButton = {
    textDecoration: 'none',
    backgroundColor: 'red',
    color: 'white',
    borderRadius: '5px',
    padding: '8px 26px',
    marginLeft: '10px',
}

const profileBody = {
    textAlign: 'center',
    paddingTop: '20px',
};

const profileCard = {
    margin: '0px auto',
    backgroundColor: 'white',
    width: '1200px',
    height: '1000px',
    borderRadius: '5px',
};

const profileCardHeader = {
    height: '120px',
    backgroundColor: 'lightgrey'
}

const iconsStyle = {
    paddingRight: '7px',
    marginTop: '45px'
}

const mapStateToProps = (state) => {
    return {
        // only needed for this component functionality properties
        loggedUser: localStorage.getItem("email"),
        isVerified: state.user.isVerified,
        isAdmin:
            state.user.isAdmin,
        isLoggedIn:
            state.user.isLoggedIn,
        password: state.user.password,
        newUser: state.user.newUser,
        errors: state.user.errors,
        loggedUserProfile: state.user.loggedUserProfile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // editProfileEmail: (email) => { dispatch(editProfileEmail(email)) },
        // editProfilePassword: (password) => { dispatch(editProfilePassword(password)) },
        // editProfileImage: (image) => { dispatch(editProfileImage(image)) },
        editProfileInfo: (infoObj) => { dispatch(editProfileInfo(infoObj)) },
        setPropertyValue: (payload) => { dispatch(setPropertyValue(payload)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);