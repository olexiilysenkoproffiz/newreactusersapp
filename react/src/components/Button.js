import React from "react";

const Button = props => {
  return (
    <button
      className={
        props.type === "primary"
          ? "btn btn-primary button-style"
          : "btn btn-secondary button-style"
      }
      onClick={props.action}
    >
      {props.title}
    </button>
  );
};

export default Button;
