import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import MyButton from "./Button";

export default class VerificationModal extends React.Component {
  render() {
    let newUserEmail = localStorage.getItem('newUserEmail')
      ? localStorage.getItem('newUserEmail')
      : 'friend@'
    const index = newUserEmail.indexOf("@");
    newUserEmail = newUserEmail.slice(0, index);
    newUserEmail = newUserEmail.charAt(0).toUpperCase() + newUserEmail.substr(1);
    const congratulations = `Dear ${newUserEmail}!
    Thank you for your email confirmation!
    Now you can log in with your
    credentials!`
    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  <h2>
                    {congratulations}
                  </h2>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"info"}
                title={"Done!"}
                style={buttonStyle}
              />
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
