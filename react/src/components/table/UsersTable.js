import React, { Component } from "react";

class UsersTable extends Component {
  render() {
    const isLoggedIn = this.props.isLoggedIn;
    const isAdmin = this.props.isAdmin;

    if (isAdmin) {
      return (
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">FirstName</th>
              <th scope="col">LastName</th>
              <th scope="col">Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users && this.props.users.map((user, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{user.firstname}</td>
                  <td>{user.lastname}</td>
                  <td>{user.email}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.onToggleEdit(user);
                      }}
                      className="btn btn-info"
                    >
                      Edit
                    </button>

                    <button
                      onClick={() => {
                        this.props.onDelete(user);
                      }}
                      className="btn btn-danger ml-2"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else if (isLoggedIn) {
      return (
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">FirstName</th>
              <th scope="col">LastName</th>
              <th scope="col">Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users && this.props.users.map((user, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{user.firstname}</td>
                  <td>{user.lastname}</td>
                  <td>{user.email}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.onToggleEdit(user);
                      }}
                      className="btn btn-info"
                    >
                      Edit
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else {
      return (
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">FirstName</th>
              <th scope="col">LastName</th>
              <th scope="col">Email</th>
              {/* <th>Action</th> */}
            </tr>
          </thead>
          <tbody>
            {this.props.users && this.props.users.map((user, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{user.firstname}</td>
                  <td>{user.lastname}</td>
                  <td>{user.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }
  }
}

export default UsersTable;
