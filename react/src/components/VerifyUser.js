import React from "react";
import Input from "./Input";

const Verify = props => {
  return (
    <div>
      <h1>Verification page works!</h1>;
      <div className="row">
        <div className="form-group col-md-10">
          {/* <label>SecretToken:</label> */}
          <Input
            autoFocus
            type={"text"}
            name={"secretToken"}
            placeholder={"Enter secretToken from the received email"}
            handleChange={props.onInput}
            error={props.error.email}
          />{" "}
        </div>
      </div>
    </div>
  );
};

export default Verify;
