import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Input from "./Input";
import MyButton from "./Button";
import { Link } from "react-router-dom";

export default class LoginModal extends React.Component {
  render() {
    return (
      <div>
        <Modal isOpen={this.props.modal}>
          <form>
            <ModalHeader>{this.props.modalHeader}</ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="form-group col-md-10">
                  <Input
                    title={"Email:"}
                    autoFocus
                    type={"text"}
                    name={"email"}
                    value={this.props.newUser.email}
                    placeholder={"Enter email"}
                    handleChange={this.props.onInput}
                    error={this.props.error && this.props.error.email}
                  />{" "}
                </div>
              </div>
              <div className="row">
                <div className="form-group col-md-10">
                  <Input
                    title={"Password:"}
                    type={"password"}
                    name={"password"}
                    value={this.props.newUser.password}
                    placeholder={"Enter password"}
                    handleChange={this.props.onInput}
                    error={this.props.error && this.props.error.password}
                  />{" "}
                </div>
              </div>
              <ul>
                <div className="modal-errors" style={errorColor}>
                  {this.props.error && Object.entries(this.props.error).map((error, index) => {
                    return <div key={index}>* {error[1]}</div>;
                  })}
                </div>
              </ul>
              <Link to="#" onClick={this.props.forgot}>
                Forgot Password?
              </Link>
            </ModalBody>
            <ModalFooter>
              <MyButton
                action={this.props.onFormSubmit}
                type={"primary"}
                title={"Login"}
                style={buttonStyle}
              />
              <Button color="danger" onClick={this.props.onToggle}>
                Cancel
              </Button>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

const errorColor = {
  color: "red",
  marginLeft: "-40px"
};
