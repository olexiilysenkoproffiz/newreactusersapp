export function validateEmail(email) {
  // eslint-disable-next-line
  const regExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const emailTested = regExp.test(String(email).toLowerCase());
  return emailTested;
}

export function isEmpty(string) {
  if (string.trim() === "") {
    return true;
  } else {
    return false;
  }
}
