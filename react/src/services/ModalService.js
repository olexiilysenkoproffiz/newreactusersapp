export const Modals = {
    modal: "modal",
    modalEdit: "modalEdit",
    modalSignUp: "modalSignUp",
    modalLogin: "modalLogin",
    modalVerify: "modalVerify",
    modalCongratulations: "modalCongratulations",
    modalReset: "modalReset",
    modalEmailSent: "modalEmailSent",
    modalForgot: "modalForgot",
}
