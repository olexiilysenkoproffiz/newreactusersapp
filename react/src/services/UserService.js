import axios from "axios";

export function createUser(userData) {
  const url = "/api/users/create";
  return axios.post(url, userData);
}

export function updateUser(payload) {
  const url = `/api/users/` + payload._id + `/update`;
  return axios.patch(url, payload);
}

export function getUsers() {
  const url = "/api/users";
  return axios.get(url);
}

export function getUserDetails(userEmail) {
  const url = `/api/userAuths/` + userEmail;
  return axios.get(url);
}

export function deleteUserService(payload, token) {
  const url = `/api/users/` + payload._id + `/delete`;
  const obj = {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    }
  };
  return fetch(url, obj);
}
