import axios from "axios";

// api endpoints for sign up and for login/refresh password
export function signUp(userData) {
  const url = "/api/userAuths/signup";
  return axios.post(url, userData);
}

export function forgotPassword(enteredEmail) {
  const url = "/api/userAuths/passwordreset";
  return axios.post(url, enteredEmail);
}

export function loginUser(userData) {
  const url = "/api/userAuths/login";
  return axios.post(url, userData);
}

export function verifyUser(enteredVerificationCode) {
  const url = "/api/userAuths/verify";
  return axios.post(url, enteredVerificationCode);
}
