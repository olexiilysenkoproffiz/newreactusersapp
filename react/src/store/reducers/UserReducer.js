const initState = {
    users: [],
    loggedUser: localStorage.getItem("email"),
    modal: false,
    modalEdit: false,
    modalForgot: false,
    modalReset: false,
    modalEmailSent: false,
    modalSignUp: false,
    modalLogin: false,
    modalVerify: false,
    modalCongratulations: false,
    isVerified: localStorage.getItem("beer") === "false" ? false : true,

    isAdmin: (localStorage.getItem("sprite") === "true" ? true : false) || false,

    isLoggedIn: (localStorage.getItem("cocaCola") === "true" ? true : false) || false,

    password: "",
    newUser: {
        firstname: "",
        lastname: "",
        email: "",
        _id: "",
        password: "",
        passwordConfirm: "",
        isAdmin: false,
        isLoggedIn: false,
        active: false,
        secretToken: ""
    },
    errors: {},
    loggedUserProfile: {
        bio: 'My Bio',
        interests: 'Edit this field',
        experience: 'Edit this field',
        profile: 'Developer',
        location: 'Lviv, Ukraine',
        experienceYears: '',
        firstname: 'Albert',
        lastname: 'Einstein',
        email: localStorage.getItem('email'),
        password: '',
        image: '',
        cv: '',
    }
}

const userReducer = (state = initState, action) => {


    // get users from db and paste them to the store
    if (action.type === "GET_USERS") {
        let users = action.payload;
        return {
            ...state,
            users
        }
    }

    // create new user
    if (action.type === "CREATE_USER") {
        let newUsers = [...state.users, action.payload]
        return {
            ...state,
            users: newUsers
        }
    }

    // delete user from state
    if (action.type === "DELETE_USER") {
        let newUsers = state.users.filter((user) => {
            return action.user._id !== user._id
        });
        return {
            ...state,
            users: newUsers
        }
    }

    // toggle any property between false or true
    if (action.type === "TOGGLE_PROPERTY") {
        const propertyName = action.payload;
        const propertyValue = !state[propertyName];
        return {
            ...state,
            [propertyName]: propertyValue
        }
    }

    // set value to any property in the state
    if (action.type === "SET_VALUE") {
        const property = action.payload.property;
        const value = action.payload.value;
        return {
            ...state,
            [property]: value
        }
    }

    if (action.type === "UPDATE_USER_INFO") {
        console.log('action.payload: ', action.payload);
        const loggedUserProfile = {
            bio: action.payload.bio,
            interests: action.payload.interests,
            experience: action.payload.experience,
            profile: action.payload.profile,
            location: action.payload.location,
            experienceYears: action.payload.experienceYears,
            firstname: action.payload.firstname,
            lastname: action.payload.lastname,
            email: action.payload.email,
            password: action.payload.password,
            image: action.payload.image,
            cv: action.payload.cv
        }
        return {
            ...state,
            loggedUserProfile
        }
    }


    return state;
}

export default userReducer;