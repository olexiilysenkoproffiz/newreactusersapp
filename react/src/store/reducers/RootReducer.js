import authReducer from './AuthReducer';
import userReducer from './UserReducer';
import { combineReducers } from 'redux';


const rootReducer = combineReducers({
    auth: authReducer,
    user: userReducer
});

export default rootReducer;