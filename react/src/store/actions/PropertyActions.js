export const toggleProperty = (property) => {
    return {
        type: 'TOGGLE_PROPERTY',
        payload: property
    }
}