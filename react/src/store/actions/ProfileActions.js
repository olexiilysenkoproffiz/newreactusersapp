import axios from 'axios';

export const editProfileEmail = (userData) => {
    return (dispatch, getState) => {
        //make async call

        dispatch({ type: 'DELETE_USER', user: userData.user })

    }
}

export const editProfilePassword = (userData) => {
    return (dispatch, getState) => {
        //make async call

        dispatch({ type: 'DELETE_USER', user: userData.user })

    }
}

export const editProfileImage = (userData) => {
    return (dispatch, getState) => {
        //make async call

        dispatch({ type: 'DELETE_USER', user: userData.user })

    }
}

export const editProfileInfo = (userInfo) => {
    return (dispatch, getState) => {

        // send new info to DB
        console.log('user info to update: ', userInfo);
        const url = "/api/userAuths/" + localStorage.getItem("email");

        axios.put(url, userInfo).then((updatedInfo) => {
            console.log('updatedInfo: ', updatedInfo.data.result.email);
            const compareEmails = updatedInfo.data.result.email === localStorage.getItem("email");
            console.log(compareEmails);
            if (compareEmails) {
                dispatch({ type: 'UPDATE_USER_INFO', payload: updatedInfo.data.result });
                window.location.reload();
            } else {
                localStorage.setItem("email", updatedInfo.data.result.email);
                //reload page
                const newLocation = 'localhost:3000/' + localStorage.getItem("email") + '/edit';
                console.log(newLocation);
                window.location.href = newLocation;
                window.location.replace(newLocation);
                // to update location link in the browser address bar on Links Profile/Edit
                window.location.reload();
            }
        });



    }
}

