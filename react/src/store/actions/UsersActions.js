import axios from 'axios';

export const deleteUser = (userData) => {
    return (dispatch, getState) => {
        //make async call
        const url = `/api/users/` + userData.user._id + `/delete`;
        const obj = {
            method: "DELETE",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + userData.token
            }
        };
        fetch(url, obj)
            .then((response) => {
                console.log("user deleted successfully!", response);
                dispatch({ type: 'DELETE_USER', user: userData.user })
            },
                err => {
                    console.log(err);
                });
    }
}

export const getUsers = () => {
    return (dispatch, getState) => {
        // make async call to db
        const url = "/api/users";
        axios.get(url).then((users) => {
            // do the normal dispatching of an action
            dispatch({ type: 'GET_USERS', payload: users.data })
        });
    }
}

export const createUserAsync = (newUser) => {
    return (dispatch, getState) => {
        const url = "/api/users/create";
        axios.post(url, newUser).then(() => {
            dispatch({ type: 'CREATE_USER', payload: newUser });
        });

    }
};

export const setPropertyValue = (payload) => {
    return {
        type: 'SET_VALUE',
        payload
    }
}