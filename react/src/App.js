import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import ResetPassword from "./components/user/ResetPassword";
import Dashboard from "./components/user/Dashboard";
import UserProfile from "./components/user/UserProfile";
import EditUserProfile from "./components/user/EditUserProfile";
import "./App.css";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/:userId/edit" component={EditUserProfile} />
          <Route path="/:userId" component={UserProfile} />
          <Route path="/:id/:token" component={ResetPassword} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
