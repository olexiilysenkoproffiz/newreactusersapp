import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import "bootstrap/dist/css/bootstrap.css";

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux';
import rootReducer from "./store/reducers/RootReducer";
import thunk from 'redux-thunk';

console.log(process.env);

// connecting the Redux;
const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
