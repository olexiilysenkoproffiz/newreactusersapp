const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let UsersAuthSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true,
    unique: true,
    match: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
  },
  password: { type: String, required: true },
  isAdmin: Boolean,
  secretToken: String,
  active: Boolean,
  isReset: Boolean,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  profile: String,
  location: String,
  experienceYears: String,
  bio: String,
  experience: String,
  interests: String,
  firstname: String,
  lastname: String,
  cv: String,
  image: String,
});

// Export the model
module.exports = mongoose.model("UserAuth", UsersAuthSchema);
