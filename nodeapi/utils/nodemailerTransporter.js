const nodemailer = require("nodemailer");
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

// send an email
const gmailPassword = process.env.GMAILPW;
const gmailUser = process.env.GMAIL_USER;

module.exports.transporter = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  secure: true,
  auth: {
    user: gmailUser,
    pass: gmailPassword
  },
  tls: { rejectUnauthorized: false } // delete on production to prevent the MitM attacks
});
