const express = require("express");
const router = express.Router();

// use file upload package
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

// apply file Filter for uploaded files

// const fileFilter = (req, file, cb) => {
//     if (file.mimetype === 'image/jpeh' || file.mimetype === 'image/png') {
//         // store the file
//         cb(null, true);
//     } else {
//         //reject a file -  not save it
//         cb(new Error('file have not been uploaded - decrease size'), false);
//     }
// };

const upload = multer({
    storage: storage,
    limits: {
        // limit filesize to 10Mb;
        fileSize: 1024 * 1024 * 100
    },
    // fileFilter: fileFilter
});

// controller userAuth
const userAuth_controller = require("../controllers/UserAuth.controller");

// sign Up
router.post("/signup", userAuth_controller.userAuth_signup);

// login
router.post("/login", userAuth_controller.userAuth_login);

// delete user
router.delete("/:userId", userAuth_controller.userAuth_delete);

// get user Profile details
router.get("/:userEmail", userAuth_controller.userAuth_details);

// edit user Profile Details
const profileUploads = upload.fields([{ name: 'image', maxCount: 1 }, { name: 'cv', maxCount: 1 }]);
// const profileUploads = upload.single('image');
router.put("/:userEmail", profileUploads, userAuth_controller.userAuth_edit_details);

// verify user
router.post("/verify", userAuth_controller.userAuth_verify);

// sent the email link to reset password window
router.post("/passwordreset", userAuth_controller.userAuth_forgot_password);

router.post("/resetpassword", userAuth_controller.userAuth_reset_password_post);

module.exports = router;
