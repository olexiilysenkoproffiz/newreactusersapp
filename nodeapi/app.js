const createError = require("http-errors");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const usersRouter = require("./routes/user");
const userAuthRoutes = require("./routes/userAuth");
const cors = require("cors");
const app = express();

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

// setting the port to use with node.js
let port = 3001;
app.listen(port, () => {
  console.log("Server is up and running on port number " + port);
});

// Set up mongoose connection
const mongoose = require("mongoose");
const dbURL = process.env.MONGO_DB_URL;
mongoose.connect(
  dbURL,
  { useNewUrlParser: true }
);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
// user createIndex deprecated issue
mongoose.set("useCreateIndex", true);

app.use(cors());

// make uploads folder publicly awailable
app.use('/uploads', express.static('uploads'));

// connect body-parser to parse the incoming request bodies in a middleware;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// defaults
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// using our routes
app.use("/api/users", usersRouter);
app.use("/api/userAuths", userAuthRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500).json({
    error: err
  });
});

module.exports = app;
