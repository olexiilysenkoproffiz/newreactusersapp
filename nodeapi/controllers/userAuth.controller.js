const _ = require("lodash");
const UserAuth = require("../models/userAuth.model");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
// simple for the password reset functionality
const jwts = require("jwt-simple");
const randomstring = require("randomstring");
const nodemailer = require("../utils/nodemailerTransporter");

// use file upload package
// const multer = require('multer');
// const upload = multer({ dest: 'uploads/' });

// SIGNUP user with POST
exports.userAuth_signup = (req, res, next) => {
  // checkout if there such email already exists
  UserAuth.find({ email: req.body.email })
    .exec()
    .then(userAuth => {
      if (userAuth.length >= 1) {
        return res.status(409).json({
          message: "Such email already exists"
        });
      }
      // need to hash our passwords with bcrypt package
      bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) {
          return res.status(500).json({
            error: err
          });
        }
        // generate the random string
        const secretToken = randomstring.generate();
        // flag the account as inactive
        const active = false;

        const userAuth = new UserAuth({
          _id: new mongoose.Types.ObjectId(),
          email: req.body.email,
          password: hash,
          secretToken: secretToken,
          active: active
        });

        userAuth
          .save()
          .then(result => {
            console.log(result);
            // generate the token from the userAuth info
            const token = jwt.sign(
              {
                email: userAuth.email,
                userId: userAuth._id
              },
              process.env.JWT_KEY,
              {
                expiresIn: "1h"
              }
            );

            // send an email
            let mailOptions = {
              from: "sfairatguardian@gmail.com",
              // email for testing - should replace with actual user email
              to: "sfairatguardian@gmail.com", // userAuth.email
              subject: "CONFIRMATION e-mail",
              text: `             Hello, my friend! You have successfully begin registration at UsersApp web-site. 
              To finish the registration, please, copy this secret code and paste it to the verification input field 
              on the web-site: 

                            ${secretToken}

              If you do not did this action just ignore this message. 
              
              Best regards,
              UsersApp team.`
            };

            nodemailer.transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                console.log(error);
              } else {
                console.log("Email sent: ", info.response);
              }
            });

            const bearer = "Bearer " + token;
            res
              .header("x-auth-token", token)
              .header("authorization", bearer)
              .header("access-control-expose-headers", "x-auth-token")
              .send(
                _.pick(userAuth, [
                  "_id",
                  "password",
                  "email",
                  "active",
                  "secretToken"
                ])
              );
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      });
    });
};

// LOGIN user
exports.userAuth_login = (req, res, next) => {
  UserAuth.findOne({ email: req.body.email })
    .exec()
    .then(userAuth => {
      console.log("userAuth from db:", userAuth);
      if (userAuth.length < 1) {
        return res.status(500).json({
          message: "Auth failed"
        });
      }
      bcrypt.compare(req.body.password, userAuth.password, (err, result) => {
        if (err) {
          return res.status(500).json({
            message: "Auth failed - wrong passs"
          });
        }
        //to check if the user is verified the email
        if (result) {
          if (userAuth.active === true) {
            const token = jwt.sign(
              {
                email: userAuth.email,
                userId: userAuth._id,
                isAdmin: userAuth.isAdmin,
                active: userAuth.active
              },
              process.env.JWT_KEY,
              {
                expiresIn: "1h"
              }
            );
            return res.status(200).json({
              message: "Auth successful",
              token: token,
              isAdmin: userAuth.isAdmin,
              active: userAuth.active
            });
          } else {
            return res.status(401).json({
              message: "Auth failed. Please sign in and verify your email."
            });
          }
        } else {
          return res.status(501).json({
            message: "Auth failed - wrong password"
          });
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(501).json({
        error: err,
        message: "Auth failed-wrong email"
      });
    });
};

// usersAuth delete userAuth
exports.userAuth_delete = (req, res) => {
  UserAuth.remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "UserAuth deleted"
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

// userAuth details send to profile page
exports.userAuth_details = function (req, res) {
  UserAuth.findOne({ email: req.params.userEmail }, function (err, user) {
    if (err) return next(err);
    // res.send(user);
    res.status(200).json({
      message: "user details",
      data: user
    });
  });
};

// edit Profile details of the user
exports.userAuth_edit_details = function (req, res, next) {
  console.log('req.file ==> ', req.files);
  console.log('request body: ', req.body);
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const email = req.params.userEmail;
  const bio = req.body.bio;
  const experience = req.body.experience;
  const location = req.body.location;
  const experienceYears = req.body.experienceYears;
  const profile = req.body.profile;
  const interests = req.body.interests;
  const image = (`uploads/` + req.files['image'][0]['originalname']) || req.body.image;
  const cv = (`uploads/` + req.files['cv'][0]['originalname']) || req.body.cv;
  // const image = req.file.path;
  // TODO: add user image, password, email, cv


  UserAuth.findOne(
    { email: email },
    (err, result) => {
      if (err) {
        throw err;
      } else if (result) {
        console.log('result ===========>   ', result);
        result.firstname = firstname;
        result.lastname = lastname;
        result.experience = experience;
        result.bio = bio;
        result.email = (req.body.email !== undefined) ? req.body.email : email;
        result.profile = profile;
        result.interests = interests;
        result.experienceYears = experienceYears;
        result.location = location;
        result.image = image;
        result.cv = cv;
        result
          .save()
          .then(result => {
            console.log("result saved to DATA-BASE: ", result);
            res.status(201).json({
              message: "User Profile properties - updated",
              result: req.body,
              files: req.files
            });
          })
          .catch(err => {
            console.log(err);
            res.status(501).json({
              message:
                "An err occures. Failed to update User Profile properties.",
              err: err
            });
          });
      } else {
        res.status(502).json({
          message:
            "An err occures. Failed to update User Profile properties."
        });
      }
    });
}

// userAuth verify
exports.userAuth_verify = function (req, res) {
  // code to verify the user
  // compare the entered secretToken with the
  // generated and saved to DB secretToken

  UserAuth.findOne({ secretToken: req.body.secretToken }, (err, result) => {
    if (err) {
      throw err;
    } else if (result) {
      result.active = true;
      result
        .save()
        .then(result => {
          console.log("result saved to DATA-BASE: ", result);
          res.status(201).json({
            message: "Auth verification passed - user can be logged in"
          });
        })
        .catch(err => {
          console.log(err);
          res.status(501).json({
            message:
              "Auth verification failed - please, enter the correct secretToken."
          });
        });
    } else {
      res.status(501).json({
        message:
          "Auth verification failed - please, enter the correct secretToken."
      });
    }
  });
};

exports.userAuth_forgot_password = function (req, res) {
  console.log("process env ", process.env.GMAIL_USER);
  // receive email from the frontend
  if (req.body.email !== undefined) {
    // Using email, find user from database.
    UserAuth.findOne({ email: req.body.email }, (err, result) => {
      if (err) {
        throw err;
      } else if (result) {
        console.log("findOne result from forgotpassword back-end: ", result);
        const payload = {
          _id: result._id,
          email: result.email
        };

        const secret = result._id + "-" + result.password;

        const token = jwts.encode(payload, secret);
        console.log("token generated: ", token);

        // send an email
        const secretToken =
          `http://localhost:3000/` + payload._id + `/` + token;

        let mailOptions = {
          from: "sfairatguardian@gmail.com",
          // email for testing - should replace with actual user email
          to: "sfairatguardian@gmail.com", // userAuth.email
          subject: "REFRESH PASSWORD",
          text: `Please, copy this secret link to your browser or simply click on it to reset your password on UsersApp : ${secretToken}`
        };

        nodemailer.transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: ", info.response);
          }
        });

        // In our case, will just return a link to click.
        res.send(
          '<a href="/resetpassword/' +
          payload.id +
          "/" +
          token +
          '">Reset password</a>'
        );
      } else {
        res.status(501).json({
          message:
            "User with such email doesn`t exist. Please, enter the correct email."
        });
        res.send("User with such email address doesn`t find in db.");
      }
    });
  } else {
    res.status(501).json({
      message: "Please, enter your email."
    });
    res.send("Email address is missing.");
  }
};

exports.userAuth_reset_password_post = function (req, res) {
  // receive the data from the body
  const _id = req.body._id;
  let password = req.body.password;
  const payload = req.body.token;

  // decode the received token

  // search the db for the user by id
  UserAuth.findOne({ _id: _id }, (err, result) => {
    if (err) {
      throw err;
    } else if (result) {
      // decode the token
      const secret = result._id + "-" + result.password;
      const token = jwts.decode(payload, secret);

      // if id match then reset the password and save
      if (_id === token._id) {
        // encode the received password
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            password = hash;
            console.log("password hashed: ", password);

            // update the user password
            UserAuth.updateOne({ _id: _id }, { $set: { password: password } })
              .exec()
              .then(result => {
                console.log(result);
                res.status(200).json({
                  message: "password updated"
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err,
                  message: "Something going wrong...try later..."
                });
              });
          }
        });
        // end of bcrypt
      } else {
        res.status(405).json({
          message: "access denied!"
        });
      }
      // end of if block
    } else {
      // if user with this id not found
      res.status(404).json({
        message: "user with such id is not found"
      });
    }
  });
};
